var gulp = require('gulp');
var browserSync = require('browser-sync').create();
var slim = require("gulp-slim");
var less = require('gulp-less');
var concat = require('gulp-concat');
var lib    = require('bower-files')();

// static Server from dist + watching every files changed
gulp.task('serve',['slim','less','scripts', 'bower:js', 'bower:css', 'fonts'], function() {
  browserSync.init({
    server: "dist",
    browser: "google chrome",    
    port: 5000,
    notify: false
  });  
  gulp.watch('app/**/*.slim', ['slim']).on('change', browserSync.reload);
  gulp.watch('app/stylesheets/**/*.less', ['less']).on('change', browserSync.reload);
  gulp.watch('app/javascripts/*.js', ['scripts']).on('change', browserSync.reload);
  gulp.watch("dist/**/*.html").on('change', browserSync.reload);
});

//gulp slim
gulp.task('slim', function(){
  gulp.src("app/**/*.slim")
    .pipe(slim({
      pretty: true,
      include: "true",
      require: 'slim/include',
      options: 'include_dirs=["./app/views/partials/"]',
      data: {
        title_doctype: "Welcome to gulp setting",
      }
    }))    
    .pipe(gulp.dest("dist"));
});

//gulp less
gulp.task('less', function (){
  gulp.src('app/stylesheets/base/*.less')
  .pipe(less())
  .pipe(gulp.dest('dist/stylesheets'));
})

//gulp font
gulp.task('fonts', function() {
  return gulp.src(['app/bower_components/slick-carousel/slick/fonts/slick.*' ])
  .pipe(gulp.dest('dist/fonts/'));
});

//gulp scripts
gulp.task('scripts', function() {
  return gulp.src(['app/javascripts/*.js'])
    .pipe(concat('main.js'))
    .pipe(gulp.dest('dist/scripts'));
});

// gulp concat bower all js
gulp.task('bower:js', function () {
  gulp.src(lib.ext('js').files)
    .pipe(concat('vendor.js'))    
    .pipe(gulp.dest('dist/scripts'))        
});

// gulp concat bower all css
gulp.task('bower:css', function () {
  gulp.src(lib.ext('css').files)
    .pipe(concat('vendor.css'))    
    .pipe(gulp.dest('dist/stylesheets'))    
});

