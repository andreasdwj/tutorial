$(document).ready(function(){
    $('.home-carousel').slick({
      dots: true,
      arrows: true,
      infinite: true,
      // slidesToShow: 5,
      // slidesToScroll: 3,
      autoplay: false,
      speed: 500,
      lazyLoad: 'ondemand',
  	  slidesToShow: 2,
  	  slidesToScroll: 1,
      centerMode: true
    });

    $('.hero-carousel').slick({
      dots: false,
      arrows: false,
      infinite: true,
      // slidesToShow: 5,
      // slidesToScroll: 3,
      autoplay: true,
      speed: 500,
      lazyLoad: 'ondemand',
	  slidesToShow: 1,
	  slidesToScroll: 1
    });

    $('.fade').slick({
      dots: true,
      infinite: true,
      speed: 500,
      fade: true,
      cssEase: 'linear'
    });

    $('.hero-slider').slick({
      dots: true,
      arrows: false,
      infinite: true,
      speed: 500,
      fade: true,
      autoplay: true,
      speed: 300,
      cssEase: 'linear'
    });

});