$(document).ready(function(){
    $('.home-carousel').slick({
      dots: true,
      arrows: true,
      infinite: true,
      // slidesToShow: 5,
      // slidesToScroll: 3,
      autoplay: false,
      speed: 500,
      lazyLoad: 'ondemand',
  	  slidesToShow: 2,
  	  slidesToScroll: 1,
      centerMode: true
    });

    $('.hero-carousel').slick({
      dots: false,
      arrows: false,
      infinite: true,
      // slidesToShow: 5,
      // slidesToScroll: 3,
      autoplay: true,
      speed: 500,
      lazyLoad: 'ondemand',
	  slidesToShow: 1,
	  slidesToScroll: 1
    });

    $('.fade').slick({
      dots: true,
      infinite: true,
      speed: 500,
      fade: true,
      cssEase: 'linear'
    });

    $('.hero-slider').slick({
      dots: true,
      arrows: false,
      infinite: true,
      speed: 500,
      fade: true,
      autoplay: true,
      speed: 300,
      cssEase: 'linear'
    });

});
$(document).ready(function(){
	// var keydown = document.getElementByClass("home-carousel")[0].onkeydown;

	// console.log(keydown);

	var modal = document.getElementById('modal-example');
	var modalbtn = $('#modal-btn');
	var span = $(".close")[0];

    $("#modal-btn").click(function(){
        console.log("masuk");
        // $("#modal-example").modal();
    });



});

$(document).ready(function(){
   $('.image-link').magnificPopup({
   	type: 'image',
    closeOnContentClick: true,
    closeBtnInside: false,
    fixedContentPos: true,
    image: {
      verticalFit: true
    },
    zoom: {
      enabled: true,
      duration: 300 // don't foget to change the duration also in CSS
    }
   });

   $('.parent-container').magnificPopup({
		  delegate: 'a', // child items selector, by clicking on it popup will open
		  type: 'image'
		  // other options
		});
});